function loadUsersData() {

  var request = new XMLHttpRequest();
  var url = "https://api.randomuser.me/1.0/?results=50&nat=gb,us&inc=gender,name,location,email,phone,picture";
  var text = request.responseText;

  request.open("GET", url, true);
  request.setRequestHeader("content-type", "application/json");
  request.onreadystatechange = function() {
    
    if (request.readyState == 4 && request.status == 200) {
      createList();
    }
  }
  request.send(null);

  function createList() {
    var contentFromUrl = JSON.parse(request.responseText);
    var listWrapper = document.createElement('td');
    
    contentFromUrl.results.forEach(function (userJSON) {
      listWrapper.appendChild(createElementOfList(userJSON));
      info.appendChild(listWrapper);
    });
    
    document.getElementById('sel').addEventListener('change', selectChanged, false);
    console.log(document.getElementById('sel'));

    function selectChanged() {
      var option = document.getElementById("sel").value;
      if (option == "1") {
        sortAZ(); sortAZ();
      }
      if (option == "2") {
        sortZA(); sortZA();
      }
    };
   
    function sortAZ(){
      contentFromUrl.results.forEach(function (userJSON) {
        listWrapper.removeChild(listWrapper.firstChild);
        contentFromUrl.results.sort(function (a, b) {
          if (a.name.last > b.name.last) {
            return 1;
          }
          if (a.name.last < b.name.last) {
            return -1;
          }
          return 0;
        });
        listWrapper.appendChild(createElementOfList(userJSON));
        info.appendChild(listWrapper);
      });
    }

    
    function sortZA() {
      contentFromUrl.results.forEach(function (userJSON) {
        listWrapper.removeChild(listWrapper.firstChild);
        contentFromUrl.results.sort(function (a, b) {
          if (a.name.last < b.name.last) {
            return 1;
          }
          if (a.name.last > b.name.last) {
            return -1;
          }
          return 0;
        });
        listWrapper.appendChild(createElementOfList(userJSON));
        info.appendChild(listWrapper);
      });
    }
    
    
  };
    

  function createElementOfList(data, index) {
    var elem = document.createElement('tr');
    elem.innerHTML = '<td><img src="' + data.picture.medium + '" /></td><td>'+ data.name.title + '</td><td>' + data.name.first + '</td><td>' + data.name.last + '</td>';
    elem.setAttribute('class', 'infoBlock');
    elem.setAttribute('data-id', index);
    elem.addEventListener("click", popUp, false);
    return elem;

    function popUp() {
      infoInWindow.removeChild(infoInWindow.lastChild);
      infoInWindow.appendChild(createElementInBlock(data));
      show('block');
      return false;
    }
    
    function createElementInBlock(data) {
      var elem = document.createElement('tr');
      elem.innerHTML = '<td><img src="' + data.picture.large + '" </td> <td><ul><li>street: '+ data.location.street + '</li>  <li> city: '+ data.location.city + '</li><li> state: ' + data.location.state  + '</li><li> email: ' + data.email + '</li><li> phone: ' + data.phone + '</li></ul></td>';
      return elem
    }
  }
}
loadUsersData();


function show(state){
  document.getElementById('window1').style.display = state;						
}

